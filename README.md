# Slack Users

This repo contains a sample iOS app that displays a list of all users on a slack team using the results of a call to https://api.slack.com/methods/users.list.

Upon tapping on a user row, the app opens the user's profile. On the individual profile page, you should see the person's picture, username, real name, and job title.

## Process

Before approaching the coding exercise, these are the steps that you usually follow:

1. Understand the problem
1. Try some examples
1. Pseudocode
1. Code the solution

### Details

#### Understanding the problem
The problem, in this case, is understanding how the slack API works and displaying the relevant information.  Part of the challenge is also to implement an iOS app that demonstrates some of the best practices.

### Trying out the API
Trying out some examples in our case involve sending some requests to the API to see the formatted response. The documentation helped identify the URL that you needed to use. This was the format of the URL:

`https://slack.com/api/users.list?token=<Insert Token ID>`

### Architecting the solution
It is good to get a general idea on how the iOS app looks and feels. Mock-ups are created for this purpose. You can see [my rough sketches/notes here](documentation/SlackApiIntegration.pdf).

The iOS app has the following layers:
- Model Layer: There are two models, one to represent the response from the slack API call and one to represent the managed objects from Core Data. A protocol `MemberData` represents the information that was needed to display in the iOS app. So both models conform to this protocol.
- Network Layer: The `NetworkManager` handles the network call while the `UserListFetcher` performs the parsing of the responses.
- UI Layer: This has two view controllers. The initial screen shows a collection view controller, which displays the list of users. Once the user taps on the individual users, a single view controller is shown to display the member profile.  The storyboard displays the general flow of the iOS app.
- Persistence Layer: We are using core data to provide offline support. When the Internet is not available, the cached data from core data is used. This requires at least one successful API call with the Internet so that the data can be stored locally for subsequent offline use.
- Localization: Add support for other languages.

You can use the issue tracker to prioritize or organize the scope of this project.  This is the [Board](https://gitlab.com/ttgs/slackusers/boards) I created for this project.

### Coding the solution

These are some notes in regards to the solution.

I broke down the tickets into manageable sized tasks and used branches to track the work in progress. The issue numbers do not necessarily indicate the order that the tasks were completed because there were tasks that were added that I thought were nice additions.

The collection view cell is done programmatically while the member profile is designed on the storyboard. The cell illustrates how you can do it if you were to choose the programmatically way to implement the UI otherwise it could have been done on the storyboard, similar to how the profile view controller is implemented.

As part of acceptance criteria, unit tests are added to at least our network layer and API response models.  I used protocols to enable dependency injection to make the iOS app more testable.

The API calls via the `NetworkManager` + `UserListFetcher` are reusable from anywhere in the iOS app if desired without being tightly coupled with the UI.

## Future enhancements

**Automation tests**

I would like to add more unit tests and UI tests for all the view controllers and persistence manager.

**Error handling**

The iOS app currently lacks error handling for the API call.  

**Image caching**

Caching the images for the session was included but it would have been nice to cache them permanently with some expiration time so that the iOS app looks better for offline support.  Alternatively, adding placeholder images could've helped as well.

**User interface**

A preview of the member profile from the user list would have been a good addition.

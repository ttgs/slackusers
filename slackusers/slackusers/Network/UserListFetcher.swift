//
//  UserListFetcher.swift
//  slackusers
//
//  Created by jchangho on 11/3/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T?)
    case failure(Error)
}

protocol UserListFetcherProtocol {
    /// Fetches the user list
    /// - Parameter completion: Result enum value with success if API
    /// returns a valid response else failure
    func fetchUserList(completion: @escaping (Result<Members?>) -> Void)
}

/// Fetcher to return a list of users.  This class confomrs to the
/// `UserListFetcherProtocol` protocol
final class UserListFetcher : UserListFetcherProtocol {

    // MARK: Private

    private let backgroundQueue: DispatchQueue = .global(qos: .background)

    // MARK: Public
    
    let networkManager: NetworkManager

    init(networkManager: NetworkManager = NetworkManager()) {
        self.networkManager = networkManager
    }

    func fetchUserList(completion: @escaping (Result<Members?>) -> Void) {

        let failureResponse: NetworkManagerError = .errorLoading
        guard let url = URL(string: SlackAPI.userListURLString) else {
            completion(.failure(failureResponse))
            return
        }

        backgroundQueue.async {
            self.networkManager.loadData(from: url, completion: { result in
                switch result {
                case .success(let data):
                    self.parseMembersData(data, completion: completion)
                case .failure(let error):
                    completion(.failure(error))
                }
            })
        }
    }

    // MARK: - Private

    private func parseMembersData(_ data: Data, completion: @escaping (Result<Members?>) -> Void) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase

        do {
            let jsonUsers = try decoder.decode(Members.self, from: data)
            completion(.success(jsonUsers))
        } catch let error {
            completion(.failure(error))
        }
    }
}

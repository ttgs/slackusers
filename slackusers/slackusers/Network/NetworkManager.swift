//
//  NetworkManager.swift
//  slackusers
//
//  Created by jchangho on 11/3/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import Foundation

protocol URLSessionProtocol {
    func loadData(from url: URL, completion: @escaping (Data?, Error?) -> Void)
}

enum NetworkResult {
    case success(Data)
    case failure(Error)
}

enum NetworkManagerError : Error {
    case errorLoading
}

final class NetworkManager {
    
    private let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func loadData(from url: URL,
                  completion: @escaping (NetworkResult) -> Void) {
        session.loadData(from: url) { data, error in
            var result: NetworkResult = .failure(NetworkManagerError.errorLoading)
            if let data = data {
                result = .success(data)
            } else if let error = error {
                result = .failure(error)
            }
            completion(result)
        }
    }
}


// MARK: - URLSessionProtocol

extension URLSession : URLSessionProtocol {
    func loadData(from url: URL, completion: @escaping (Data?, Error?) -> Void) {
        let task = dataTask(with: url) { (data, _, error) in
            completion(data, error)
        }
        task.resume()
    }
}

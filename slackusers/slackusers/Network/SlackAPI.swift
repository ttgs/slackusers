//
//  SlackAPI.swift
//  slackusers
//
//  Created by jchangho on 11/3/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import Foundation

struct SlackAPI {
    
    // MARK: Private
    
    private struct Constant {
        static let slackTokenID = "xoxp-4698769766-4698769768-441940351876-4c67ca89a4f5c56cdd81ea5506bf1d13"
        static let baseAPI = "https://slack.com/api"
        static let userListPath = "/users.list"
    }
    
    // MARK: Public

    /// String URL for fetching user list
    static let userListURLString: String = {
        return String(format: "%@%@?token=%@", Constant.baseAPI, Constant.userListPath, Constant.slackTokenID)
    }()
}

//
//  MembersCollectionViewController.swift
//  slackusers
//
//  Created by jchangho on 11/4/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import UIKit

/// List of users
final class MembersCollectionViewController : UICollectionViewController {

    // MARK: Private

    struct Constant {
        static let numberOfSections = 1
        static let memberCellID = "MemberCellID"
        static let cellHeight: CGFloat = 90
        static let minimumSpacing: CGFloat = 1
    }

    private let fetcher: UserListFetcherProtocol
    private(set) var members: [MemberData] = []
    private let persistenceManager: PersistenceManager

    // MARK: Public

    init(fetcher: UserListFetcherProtocol = UserListFetcher(),
         persistenceManager: PersistenceManager = PersistenceManager.shared) {
        self.fetcher = fetcher
        self.persistenceManager = persistenceManager
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.fetcher = UserListFetcher()
        self.persistenceManager = PersistenceManager.shared
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = NSLocalizedString("membersList.title", comment: "Members Page Title")
        collectionView.register(MemberCollectionViewCell.self, forCellWithReuseIdentifier: Constant.memberCellID)
        collectionView.backgroundColor = .black
        
        displayMembers()
    }

    // MARK: - Private

    private func displayMembers() {
        let coreData = CoreDataController(persistenceManager: persistenceManager)
        fetcher.fetchUserList { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let members):
                    self?.members = members!!.members ?? []
                    self?.collectionView.reloadData()
                    coreData.deleteMembers()
                    coreData.storeData(members: self?.members)
                case .failure(let error):
                    print(error.localizedDescription)
                    self?.members = coreData.retrieveMembers() ?? []
                    self?.collectionView.reloadData()
                }
            }
        }
    }
}


// MARK: - UICollectionViewDelegateFlowLayout

extension MembersCollectionViewController : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: Constant.cellHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constant.minimumSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constant.minimumSpacing
    }
}


// MARK: - UICollectionViewDataSource

extension MembersCollectionViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Constant.numberOfSections
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return members.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.memberCellID, for: indexPath) as! MemberCollectionViewCell
        cell.configureCell(member: members[indexPath.row])
        return cell
    }
}


// MARK: - UICollectionViewDelegate

extension MembersCollectionViewController {

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        viewController.member = members[indexPath.row]
        navigationController?.pushViewController(viewController, animated: true)
    }
}

//
//  ProfileViewController.swift
//  slackusers
//
//  Created by jchangho on 11/4/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import UIKit

class ProfileViewController : UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userNameValue: UILabel!
    @IBOutlet weak var realNameLabel: UILabel!
    @IBOutlet weak var realNameValue: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var jobTitleValue: UILabel!

    var member: MemberData?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("profile.title", comment: "Profile Page Title")
        setUpView()
    }

    // MARK: Private

    private func setUpView() {
        guard let member = member else {
            userNameValue.text = nil
            realNameValue.text = nil
            jobTitleLabel.text = nil
            return
        }

        loadImage(member)
        configureLabels(member)
    }

    private func loadImage(_ member: MemberData) {
        guard let imageURLString = member.image192,
            let imageURL = URL(string: imageURLString) else {
                return
        }

        imageView.load(url: imageURL)
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = imageView.frame.width / 2
        imageView.clipsToBounds = true
    }

    private func configureLabels(_ member: MemberData) {
        userNameLabel.text = NSLocalizedString("profile.userName.label", comment: "Display name label").uppercased()
        realNameLabel.text = NSLocalizedString("profile.realName.label", comment: "Real name label").uppercased()
        jobTitleLabel.text = NSLocalizedString("profile.jobTitle.label", comment: "Job title label").uppercased()

        let notApplicableText = NSLocalizedString("general.notApplicable.label", comment: "Not applicable")
        userNameValue.text = member.displayName ?? notApplicableText
        realNameValue.text = member.realName
        jobTitleValue.text = member.title ?? notApplicableText
    }
}

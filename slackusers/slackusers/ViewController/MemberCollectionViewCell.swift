//
//  MemberCollectionViewCell.swift
//  slackusers
//
//  Created by jchangho on 11/4/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import UIKit

/// Member cell to display each individual profile image and name
class MemberCollectionViewCell : UICollectionViewCell {

    // MARK: Private
    private(set) var profileImage = UIImageView()
    private(set) var nameLabel = UILabel(frame: .zero)

    private struct Constant {
        static let imageSize = CGSize(width: 72, height: 72)
        static let spacing: CGFloat = 8
        static let textColor: UIColor = .white
    }

    // MARK: - Public

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpCell()
    }

    required init?(coder aDecoder: NSCoder) {
        return nil
    }

    func configureCell(member: MemberData) {
        configureProfileImage(member: member)
        configureLabels(member: member)
    }

    // MARK: - Private

    private func setUpCell() {
        contentView.addSubview(profileImage)
        contentView.addSubview(nameLabel)

        profileImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            profileImage.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Constant.spacing),
            profileImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            profileImage.heightAnchor.constraint(equalToConstant: Constant.imageSize.height),
            profileImage.widthAnchor.constraint(equalToConstant: Constant.imageSize.width),
            ])
        profileImage.layer.cornerRadius = 5
        profileImage.clipsToBounds = true

        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: Constant.spacing),
            nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])

        backgroundColor = .gray
    }

    private func configureProfileImage(member: MemberData) {
        guard let imageURLString = member.image72,
            let imageURL = URL(string: imageURLString) else {
            return
        }

        profileImage.load(url: imageURL)
    }

    private func configureLabels(member: MemberData) {
        nameLabel.text = member.realName.capitalized
        nameLabel.textColor = Constant.textColor
    }
}

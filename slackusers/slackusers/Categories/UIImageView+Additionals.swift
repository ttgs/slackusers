//
//  UIImageView+Additionals.swift
//  slackusers
//
//  Created by jchangho on 11/4/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func load(url: URL) {

        // check cached image before downloading
        let urlString = url.absoluteString
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }

        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        imageCache.setObject(image, forKey: urlString as AnyObject)
                        self?.image = image
                    }
                }
            }
        }
    }
}

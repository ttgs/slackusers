//
//  Member.swift
//  slackusers
//
//  Created by jchangho on 11/3/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import Foundation

// MARK: - Members

struct Members : Codable {
    var members: [Member]?
}


// MARK: - Member

struct Member : Codable {

    var id: String
    var teamId: String
    var name: String
    var deleted: Bool
    var color: String
    var realName: String
    var tz: String?
    var tzLabel: String
    var tzOffset: Int
    var profile: Profile
    var isAdmin: Bool
    var isOwner: Bool
    var isPrimaryOwner: Bool
    var isRestricted: Bool
    var isUltraRestricted: Bool
    var isBot: Bool
    var updated: Int64
    var isAppUser: Bool
    var has2fa: Bool?
}


// MARK: - Profile

struct Profile : Codable {
    var avatarHash: String?
    var statusText: String?
    var statusEmoji: String?
    var realName: String?
    var displayName: String?
    var realNameNormalized: String?
    var displayNameNormalized: String?
    var title: String?
    var email: String?
    var image24: String?
    var image32: String?
    var image48: String?
    var image72: String?
    var image192: String?
    var image512: String?
    var imageOriginal: String?
    var team: String?
}


// MARK: - MemberData

extension Member: MemberData {
    var displayName: String? {
        return profile.displayName
    }

    var title: String? {
        return profile.title
    }

    var image72: String? {
        return profile.image72
    }

    var image192: String? {
        return profile.image192
    }
}

//
//  MemberData.swift
//  slackusers
//
//  Created by jchangho on 11/4/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import Foundation

protocol MemberData {
    var displayName: String? { get }
    var realName: String { get }
    var title: String? { get }
    var image72: String? { get }
    var image192: String? { get }
}

struct MemberCoreData : MemberData {
    var displayName: String?
    var realName: String
    var title: String?
    var image72: String?
    var image192: String?
}

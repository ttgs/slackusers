//
//  MemberEntity+CoreDataProperties.swift
//  slackusers
//
//  Created by jchangho on 11/4/18.
//  Copyright © 2018 TTGS. All rights reserved.
//
//

import Foundation
import CoreData


extension MemberEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MemberEntity> {
        return NSFetchRequest<MemberEntity>(entityName: "MemberEntity")
    }

    @NSManaged public var displayName: String?
    @NSManaged public var image72: String?
    @NSManaged public var image192: String?
    @NSManaged public var realName: String?
    @NSManaged public var title: String?

}

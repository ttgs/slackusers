//
//  CoreDataController.swift
//  slackusers
//
//  Created by jchangho on 11/4/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

import CoreData
import Foundation

final class CoreDataController {

    // MARK: Private

    private enum KeyPath: String {
        case displayName = "displayName"
        case realName = "realName"
        case title = "title"
        case image72 = "image72"
        case image192 = "image192"
    }

    private struct Constant {
        static let memberEntityName = "MemberEntity"
    }

    // MARK: Public

    let persistenceManager: PersistenceManager

    init(persistenceManager: PersistenceManager = PersistenceManager.shared) {
        self.persistenceManager = persistenceManager
    }

    func storeData(members: [MemberData]?) {
        guard let members = members else {
                return
        }

        let managedContext = persistenceManager.context
        guard let memberEntity = NSEntityDescription.entity(forEntityName: Constant.memberEntityName, in: managedContext) else {
            return
        }

        for member in members {
            let memberEntity = NSManagedObject(entity: memberEntity, insertInto: managedContext)
            memberEntity.setValue(member.displayName, forKeyPath: KeyPath.displayName.rawValue)
            memberEntity.setValue(member.realName, forKeyPath: KeyPath.realName.rawValue)
            memberEntity.setValue(member.title, forKeyPath: KeyPath.title.rawValue)
            memberEntity.setValue(member.image72, forKeyPath: KeyPath.image72.rawValue)
            memberEntity.setValue(member.image192, forKeyPath: KeyPath.image192.rawValue)
        }

        persistenceManager.save()
    }

    func retrieveMembers() -> [MemberData]? {
        let managedContext = persistenceManager.context
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constant.memberEntityName)

        do {
            let result = try managedContext.fetch(fetchRequest)
            var resultMemberData: [MemberData] = []
            for data in result as! [NSManagedObject] {
                let displayName = data.value(forKey: KeyPath.displayName.rawValue) as! String
                let realName = data.value(forKey: KeyPath.realName.rawValue) as! String
                let title = data.value(forKey: KeyPath.title.rawValue) as? String
                let image72 = data.value(forKey: KeyPath.image72.rawValue) as? String
                let image192 = data.value(forKey: KeyPath.image192.rawValue) as? String
                let member = MemberCoreData(displayName: displayName,
                                            realName: realName,
                                            title: title,
                                            image72: image72,
                                            image192: image192)
                resultMemberData.append(member)
            }
            return resultMemberData
        } catch let error as NSError {
            print("Failed to retrieve data. \(error), \(error.userInfo)")
        }

        return nil
    }

    func deleteMembers() {
        let managedContext = persistenceManager.context
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constant.memberEntityName)

        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try managedContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print("Failed to remove data. \(error), \(error.userInfo)")
        }
    }
}

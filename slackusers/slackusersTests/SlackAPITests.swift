//
//  SlackAPITests.swift
//  slackusersTests
//
//  Created by jchangho on 11/3/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

@testable import slackusers
import XCTest

final class SlackAPITests : XCTestCase {
    
    func testUserList() {
        XCTAssertEqual(SlackAPI.userListURLString, "https://slack.com/api/users.list?token=xoxp-4698769766-4698769768-441940351876-4c67ca89a4f5c56cdd81ea5506bf1d13")
    }
}

//
//  UserListFetcherTests.swift
//  slackusersTests
//
//  Created by jchangho on 11/3/18.
//  Copyright © 2018 TTGS. All rights reserved.
//

@testable import slackusers
import XCTest

final class UserListFetcherTests : XCTestCase {

    func testMembersCodable() throws {
        let jsonData = try userListData()
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase

        let result: Members = try decoder.decode(Members.self, from: jsonData!)
        XCTAssertEqual(result.members!.count, 8)
    }

    func testFetcherSuccess() throws {
        let jsonData = try userListData()
        let session = MockURLSession()
        session.data = jsonData
        let networkManager = NetworkManager(session: session)
        let fetcher = UserListFetcher(networkManager: networkManager)

        fetcher.fetchUserList { result in
            switch result {
            case .success(let data):
                XCTAssertEqual(data!!.members!.count, 8)
            case .failure(_):
                XCTFail("Failure not expected")
            }
        }
    }

    func testFetcherFailure() throws {
        let session = MockURLSession()
        session.error = NetworkManagerError.errorLoading
        let networkManager = NetworkManager(session: session)
        let fetcher = UserListFetcher(networkManager: networkManager)

        fetcher.fetchUserList { result in
            switch result {
            case .success(_):
                XCTFail("Success is not expected")
            case .failure(let error):
                XCTAssertEqual(error as! NetworkManagerError, NetworkManagerError.errorLoading)
            }
        }
    }

    private func userListData() throws -> Data? {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "userlists", withExtension: "json") else {
            XCTFail("Missing file: userlists.json")
            return nil
        }
        return try Data(contentsOf: url)
    }
}


// MARK: - MockURLSession

final class MockURLSession : URLSessionProtocol {

    var data: Data?
    var error: Error?

    func loadData(from url: URL, completion: @escaping (Data?, Error?) -> Void) {
        completion(data, error)
    }
}
